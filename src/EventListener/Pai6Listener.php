<?php

namespace App\EventListener;

use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class Pai6Listener
{
    public function onKernelRequest(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $session = $request->getSession();

        if (!$session->has('lang'))
            $session->set('lang', 'pl');


        // some logic to determine the $locale
        $request->setLocale($session->get('lang'));
    }
}
