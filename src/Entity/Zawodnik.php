<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ZawodnikRepository")
 */
class Zawodnik
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $imie;

    /**
     * @ORM\Column(type="date")
     */
    private $data_urodzin;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getImie(): ?string
    {
        return $this->imie;
    }

    public function setImie(string $imie): self
    {
        $this->imie = $imie;

        return $this;
    }

    public function getDataUrodzin(): ?\DateTimeInterface
    {
        return $this->data_urodzin;
    }

    public function setDataUrodzin(\DateTimeInterface $data_urodzin): self
    {
        $this->data_urodzin = $data_urodzin;

        return $this;
    }
}
