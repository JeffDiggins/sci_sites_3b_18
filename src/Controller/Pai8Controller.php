<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Pai8Controller extends AbstractController
{
    /**
     * @Route("/pai/8", name="pai8")
     */
    public function index()
    {
        $results['1.1'] = $this->dateDiff('2018-12-11 10:11', '2018-12-11 10:25', 'i');
        $results['1.2'] = $this->dateDiff('2018-12-13 09:17', '2018-12-13 09:35', 'i');
        $results['1.3'] = $this->dateDiff('2018-12-12 10:16:00', '2018-12-12 10:25:00', 'i');
        $results['1.4'] = $this->dateDiff('2018-12-11 10:11', '2018-12-14 16:40', 'i');
        $results['1.5'] = $this->dateDiff('2018-12-13 09:17', '2018-12-14 13:10', 'i');
        $results['1.6'] = $this->dateDiff('2018-12-12 10:16:00', '2018-12-14 16:40:00', 'i');
        $results['2.1'] = $this->dateDiff('2018-12-11', '2018-12-19', 'a');
        $results['2.2'] = $this->dateDiff('2018-12-13', '2018-12-19', 'a');
        $results['2.3'] = $this->dateDiff('2018-12-12', '2018-12-19', 'a');
        $results['3.1'] = $this->dateDiff('2018-12-10', '2019-06-23', 'a');
        $results['3.2'] = $this->dateDiff('2018-12-13', '2019-06-23', 'a');
        $results['3.3'] = $this->dateDiff('2018-12-12', '2019-06-23', 'a');
        $results['4'] = $this->dateDiff('1999-04-15', 'now', 'a');
        $results['5'] = (new \DateTime('now'))->add(new \DateInterval('P90D'))->format('Y-m-d H:i');
        return $this->render('pai8/index.html.twig', array(
            'results' => $results
        ));
    }

    private function dateDiff (String $dateString1, String $dateString2, String $retVal){
        $dateTime1 = new \DateTime($dateString1);
        $dateTime2 = new \DateTime($dateString2);
        if ($retVal == 'a')
            return $dateTime1->diff($dateTime2)->format('%a days');
        elseif ($retVal == 'i')
        {
            return ((new \DateTime())->setTimeStamp(0)->add($dateTime1->diff($dateTime2))->getTimeStamp())/60 . ' minutes';
        }
    }
}
