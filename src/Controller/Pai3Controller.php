<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Dotenv\Dotenv;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Pai3Controller extends AbstractController
{
    /**
     * @Route("/pai/3", name="pai3")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();
        if (!$session->get('tries'))
            $session->set('tries', 0);

        if (!$session->get('timeout'))
            $session->set('timeout', 0);

        if ($session->get('tries') >= 3){
            $session->set('tries', 0);
            $session->set('timeout', time() + 60);
        }

        if (!$session->has('login')) {
            $form = $this->createFormBuilder()
                ->add('login', TextType::class)
                ->add('password', PasswordType::class, array(
                    'label' => 'Hasło'
                ))
                ->add('submit', SubmitType::class, array(
                    'label' => 'Zaloguj'
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid() && $session->get('timeout') < time()) {
                $dotenv = new Dotenv();
                $dotenv->load(__DIR__.'/../../.env');
                $session = $request->getSession();
                if (
                    $form->getData()['login'] == getenv('ADMIN_LOGIN') &&
                    $form->getData()['password'] == getenv('ADMIN_PASSWORD')
                ) {
                    $session->set('login', $form->getData()['login']);
                    $session->set('password', $form->getData()['password']);
                    $message = 'Zalogowano pomyślnie';
                }
                else{
                    $message = 'Nieudana próba logowania';
                    $session->set('tries', $session->get('tries') + 1);
                }
                return $this->render('pai3/login.html.twig', array(
                    'message' => $message,
                    'data' => $form->getData()
                ));
            }
        }
        else {
            $form = $this->createFormBuilder()
                ->add('submit', SubmitType::class, array(
                    'label' => 'Wyloguj'
                ))
                ->getForm();

            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $session->invalidate();
                return $this->redirectToRoute('pai3');
            }
        }

        return $this->render('pai3/index.html.twig', [
            'form' => $form->createView(),
            'session' => $session
        ]);
    }
}
