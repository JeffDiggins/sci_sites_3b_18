<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class WiaController extends AbstractController
{
    /**
     * @Route("/wia", name="wia")
     */
    public function index()
    {
        return $this->render('wia/index.html.twig');
    }
    /**
     * @Route("/wia/{num}", name="wiaNum")
     */
    public function wia($num)
    {
        return $this->render('wia/wia' . $num . '_oskar_rodziewicz_3b_18_sci.html');
    }
    /**
     * @Route("/wia/9", name="e14")
     */
    public function e14()
    {
        return $this->render('wia/wia9/portal.html');
    }
}
