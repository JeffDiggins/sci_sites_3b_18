<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class PaiController extends AbstractController
{
    /**
     * @Route("/pai", name="pai")
     */
    public function index()
    {
        return $this->render('pai/index.html.twig', [
            'controller_name' => 'PaiController',
        ]);
    }
    /**
     * @Route("/pai/{num}", name="paiNum")
     */
    public function pai($num)
    {
        return $this->redirectToRoute('pai' . $num);
    }
}
