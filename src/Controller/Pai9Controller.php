<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\KernelInterface;

class Pai9Controller extends AbstractController
{
    /**
     * @Route("/pai/9", name="pai9")
     */
    public function index(KernelInterface $kernel)
    {
        function prepToOut($arr)
        {
            return array_map(
                function ($val) {
                    return str_replace('.', ',', $val .= PHP_EOL);
                },
                array_map('strval', $arr));
        }
        $nums = array_map(
            function ($val) {
                return floatval(str_replace(',', '.', $val));
            },
            file($kernel->getProjectDir() . '/public/pai9/liczby.txt', FILE_IGNORE_NEW_LINES)
        );
        $data['max'] = max($nums);
        $data['min'] = min($nums);
        $data['avg'] = array_sum($nums) / count($nums);
        file_put_contents(
            $kernel->getProjectDir() . '/public/pai9/wynik2.txt',
            prepToOut(array_filter($nums, function ($num) {
                return (($num >= 10 && $num < 100) || ($num <= -10 && $num > -100)) && $num % 3 == 0;
            }))
        );
        $numsSort = $nums;
        sort($numsSort);
        file_put_contents(
            $kernel->getProjectDir() . '/public/pai9/sort.txt',
            prepToOut($numsSort)
        );
        return $this->render('pai9/index.html.twig', array(
            'data' => $data,
        ));
    }


}
