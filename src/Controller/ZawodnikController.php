<?php

namespace App\Controller;

use App\Entity\Zawodnik;
use App\Form\ZawodnikType;
use App\Repository\ZawodnikRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/zawodnik")
 */
class ZawodnikController extends AbstractController
{
    /**
     * @Route("/", name="zawodnik_index", methods={"GET"})
     */
    public function index(ZawodnikRepository $zawodnikRepository): Response
    {
        return $this->render('zawodnik/index.html.twig', [
            'zawodniks' => $zawodnikRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="zawodnik_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $zawodnik = new Zawodnik();
        $form = $this->createForm(ZawodnikType::class, $zawodnik);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($zawodnik);
            $entityManager->flush();

            return $this->redirectToRoute('zawodnik_index');
        }

        return $this->render('zawodnik/new.html.twig', [
            'zawodnik' => $zawodnik,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="zawodnik_show", methods={"GET"})
     */
    public function show(Zawodnik $zawodnik): Response
    {
        return $this->render('zawodnik/show.html.twig', [
            'zawodnik' => $zawodnik,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="zawodnik_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Zawodnik $zawodnik): Response
    {
        $form = $this->createForm(ZawodnikType::class, $zawodnik);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('zawodnik_index', [
                'id' => $zawodnik->getId(),
            ]);
        }

        return $this->render('zawodnik/edit.html.twig', [
            'zawodnik' => $zawodnik,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="zawodnik_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Zawodnik $zawodnik): Response
    {
        if ($this->isCsrfTokenValid('delete'.$zawodnik->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($zawodnik);
            $entityManager->flush();
        }

        return $this->redirectToRoute('zawodnik_index');
    }
}
