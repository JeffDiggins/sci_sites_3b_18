<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class Pai13Controller extends AbstractController
{
    /**
     * @Route("/pai/13", name="pai13")
     */
    public function index(Request $request, KernelInterface $kernel)
    {
        $form = $this->createFormBuilder()
            ->add('class', ChoiceType::class, array(
                'label' => 'Klasa',
                'choices' => array(
                    '3A' => '3a',
                    '3B' => '3b',
                    '3C' => '3c',
                ),
            ))
            ->add('name', TextType::class, array(
                'label' => 'Nazwa wydarzenia',
            ))
            ->add('startDate', DateType::class, array(
                'label' => 'Termin dodania',
                'data' => new \DateTime()
            ))
            ->add('endDate', DateType::class, array(
                'label' => 'Termin oddania',
                'data' => new \DateTime()
            ))
            ->add('days', HiddenType::class, array(
                'data' => 0
            ))
            ->add('difficulty', ChoiceType::class, array(
                'label' => 'Stopień trudności',
                'choices' => array(
                    'Łatwy' => '1',
                    'Średni' => '2',
                    'Trudny' => '3',
                )
            ))
            ->add('submit', SubmitType::class)
            ->addEventListener(FormEvents::SUBMIT, function (FormEvent $event){
                $data = $event->getData();
                $data['startDate'] = $data['startDate']->format('Y-m-d');
                $data['endDate'] = $data['endDate']->format('Y-m-d');
                $event->setData($data);
            })
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $handle = fopen($kernel->getProjectDir() . '/public/pai9/Terminarz-PAI-3.csv', "a");
            fputcsv($handle, $form->getData());
            fclose($handle);
            return $this->redirectToRoute('pai13');
        }
        return $this->render('pai13/index.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
