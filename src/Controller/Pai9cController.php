<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Stopwatch\Stopwatch;

class Pai9cController extends AbstractController
{
    /**
     * @Route("/pai/9c", name="pai9c")
     */
    public function index(KernelInterface $kernel)
    {
        $handle = fopen($kernel->getProjectDir() . '/public/pai9/Terminarz-PAI-3.csv', "r");
        $header = fgetcsv($handle, 1337);
        $data = [null];
        while ($line = fgetcsv($handle, 2137)) {
            $line[4] = ((new \DateTime())->setTime(0,0,0))->diff(new \DateTime($line[3]))->format('%r%a');
            $line[0] !== '3b' ?: ($line[4] >= 0 ? $data[0][] = $line : $data[1][] = $line);
        }
        fclose($handle);
        foreach ($data as &$table)
            usort($table, function ($a, $b) {return ($a[2] < $b[2]) ? 1 : -1;});
        return $this->render('pai9c/index.html.twig', [
            'data' => $data,
            'header' => $header
        ]);
    }
}
