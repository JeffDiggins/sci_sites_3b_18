<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Pai15Controller extends AbstractController
{
    /**
     * @Route("/pai/15", name="pai15")
     */
    public function index()
    {
        dump($this->f6(3));
        exit;
        return $this->render('pai15/index.html.twig', [
            'controller_name' => 'Pai15Controller',
        ]);
    }

    public function f1($num){return $num % 7 === 0;}
    public function f2($a, $b = 1){return $a / $b;}
    public function f3($str){return 2*strlen($str);}
    public function f4($num){return array_sum(str_split($num));}
    public function f5($str){return substr_count($str, ' ');}
    public function f6($num){return $this->f7($num, $num);}
    public function f7($rows, $cols){
        $rows = $rows < 3 ? 3 : $rows;
        $cols = $cols < 3 ? 3 : $cols;
        $table = '<table>';
        for ($i = 0; $i < $rows; $i++){
            $table .= '<tr>';
            for ($j = 0; $j < $cols; $j++){
                $table .= '<td>';
                $table .= '</td>';
            }
            $table .= '</tr>';
        }
        $table .= '</table>';
        return $table;
    }
}
