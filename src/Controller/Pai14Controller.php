<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Sabre\VObject;

class Pai14Controller extends AbstractController
{
    /**
     * @Route("/pai14", name="pai14")
     */
    public function index(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('file', FileType::class)
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $file = $form->getData()['file'];
            $mime = $file->getClientMimeType();
            $data = [];
            if ($mime == 'text/csv' || $mime == 'application/vnd.ms-excel' || $mime == 'text/plain')
            {
                $handle = fopen($file, "r");
                fgetcsv($handle, 1337);
                while ($line = fgetcsv($handle, 2137)) {
                    if (count($line) == 6) {
                        $line[4] = $line[5];
                        unset($line[5]);
                    }
                    $line[0] !== '3b' ?: $data[] = $line;
                }
                fclose($handle);
                if (count($data) == 0 || count($data[0]) != 5)
                    throw new BadRequestHttpException();
            }
            elseif ($mime == 'application/x-sqlite3' || $mime == 'application/octet-stream')
            {
                $db = new \PDO('sqlite:' . $file);
                if (($query = $db->query("SELECT * FROM terminarz")) === false)
                    throw new BadRequestHttpException();
                $data = $query->fetchAll();
            }
            elseif ($mime == 'application/json')
            {

            }
            else{
                throw new BadRequestHttpException();
            }
            $ics = new VObject\Component\VCalendar();
            foreach ($data as $item){
                $ics->add('VEVENT', [
                    'SUMMARY' => $item[1],
                    'DTSTART' => new \DateTime($item[2]),
                    'DTEND' => new \DateTime($item[3]),
                    'ORGANIZER' => 'SCI',
                    'STATUS' => 'CONFIRMED',
                ]);
            }
            $response = new Response($ics->serialize());
            $response->headers->set(
                'Content-Disposition',
                $response->headers->makeDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, 'calendar.ics')
            );
            return $response;
        }
        return $this->render('pai14/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
