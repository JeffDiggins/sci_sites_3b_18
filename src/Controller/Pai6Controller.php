<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Intl\Intl;

class Pai6Controller extends AbstractController
{
    /**
     * @Route("/pai/6", name="pai6")
     */
    public function index(Request $request)
    {
        $session = $request->getSession();

        $contentLang = array('de', 'en', 'pl', 'fr', 'ru');
        $formChoices = array_intersect(array_flip(Intl::getLanguageBundle()->getLanguageNames()), $contentLang);

        $details = json_decode(file_get_contents("http://ipinfo.io/".$request->server->get('REMOTE_ADDR')."/json"));
        $details2 = json_decode(file_get_contents("http://ipinfo.io/json"));

        $country = array();
        if (isset($details->country))
            $country = array(strtolower($details->country));

        $form = $this->createFormBuilder()
            ->add('lang', LanguageType::class, array(
                'choices' => $formChoices,
                'choice_loader' => null,
                'preferred_choices' => $country
            ))
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $session->set('lang', $form->getData()['lang']);
            $session->set('langUserSet', 1);
            return $this->redirectToRoute('pai6');
        }

        if ($session->has('langUserSet') && !$session->get('langUserSet')) {
            $langInit = explode(',', $request->server->get('HTTP_ACCEPT_LANGUAGE'));
            $detectedLang = array();
            foreach ($langInit as $item)
            {
                $item = explode(';', $item);
                $item = reset($item);
                $item = explode('-', $item);
                $item = reset($item);
                $detectedLang[] = $item;
            }
            $detectedLang = array_unique($detectedLang);

            foreach ($detectedLang as $item)
            {
                if (in_array($item, $contentLang))
                {
                    $session->set('lang', $item);
                    break;
                }
            }
        }


        return $this->render('pai6/index.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
