<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class Pai10Controller extends AbstractController
{
    /**
     * @Route("/pai10", name="pai10")
     */
    public function index()
    {
        return $this->render('pai10/index.html.twig', [
            'controller_name' => 'Pai10Controller',
        ]);
    }
}
