<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Routing\Annotation\Route;

class WiaOwoceController extends AbstractController
{
    /**
     * @Route("/wia_owoce", name="wia_owoce")
     */
    public function index()
    {
        return $this->render('wia_owoce/index.html', array());
    }
}
