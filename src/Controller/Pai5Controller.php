<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\HttpKernel\Exception\HttpException;

class Pai5Controller extends AbstractController
{
    /**
     * @Route("/pai/5", name="pai5")
     */
    public function index(Request $request)
    {
        if (strpos($request->server->get('HTTP_ACCEPT'), 'application/xml') !== false)
        {
            $encoder = new XmlEncoder();
            $response = new Response($encoder->encode(array('foo' => array('@bar' => 'value')), 'xml'));
            $response->headers->set('Content-Type', 'xml');
            return $response;
        }
        else
        {
            throw new HttpException(418, 'Cannot return an XML response.');
        }

    }
}
