<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class Pai4Controller extends AbstractController
{
    /**
     * @Route("/pai/4", name="pai4")
     */
    public function index(Request $request)
    {
        $form = $this->createFormBuilder()
            ->add('firstName', TextType::class, array(
                'label' => '*Imie',
            ))
            ->add('lastName', TextType::class, array(
                'label' => '*Nazwisko',
            ))
            ->add('email', EmailType::class, array(
                'label' => '*Adres E-mail',
                'attr' => array(
                    'placeholder' => 'example@example.com',
                )
            ))
            ->add('birthdate', BirthdayType::class, array(
                'label' => 'Data urodzenia',
                'placeholder' => '',
                'required' => false
            ))
            ->add('sex', ChoiceType::class, array(
                'label' => '*Płeć',
                'choices' => array(
                    'Mężczyzna' => 'Mężczyzna',
                    'Kobieta' => 'Kobieta'
                ),
                'expanded' => true,
            ))
            ->add('terms', CheckboxType::class, array(
                'label' => '*Zapoznałem się z treścią regulaminu',
            ))
            ->add('rodo', CheckboxType::class, array(
                'label' => 'Wiem na czym polega RODO',
                'required' => false,
            ))
            ->add('sci', CheckboxType::class, array(
                'label' => 'Oświadczam, że lubię SCI i sprzedałem/am duszę diabłu',
                'required' => false,
            ))
            ->add('topic', TextType::class, array(
                'label' => 'Temat',
            ))
            ->add('text', TextareaType::class, array(
                'label' => 'Treść',
            ))
            ->add('submit', SubmitType::class)
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
//            dump($form->getData());
//            exit;
            $transport = (new \Swift_SmtpTransport('poczta.o2.pl', 465, 'ssl'))
                ->setUsername('testmailxd@o2.pl')
                ->setPassword('zaq1@WSX');
            $mailer = new \Swift_Mailer($transport);

            $message = (new \Swift_Message($form->getData()['topic']))
                ->setFrom('testmailxd@o2.pl')
                ->setTo($form->getData()['email'])
                ->setBody(
                    $this->renderView(
                        'pai4/email.html.twig',
                        array('data' => $form->getData())
                    ),
                    'text/html'

                )
                ->addPart(
                    $this->renderView(
                        'pai4/email.html.twig',
                        array('data' => $form->getData())
                    ),
                    'text/plain'
                );

            $mailer->send($message);
        }

        return $this->render('pai4/index.html.twig', array(
            'form' => $form->createView()
        ));
    }
}
