class Pung {
  constructor(canvasId, customOpts) {
    this.canvas = document.getElementById(canvasId);
    this.ctx = this.canvas.getContext('2d');

    this.canvas.oldWidth = this.canvas.width;
    this.canvas.oldHeight = this.canvas.height;

    this.setOpts(customOpts);

    this.menu = new Menu(this);
    this.game = new Game(this);
    this.pause = new Pause(this);

    this.activeStateReal = this.menu;

    this.lastTickTime = 0;

    this.buttons = {
      fullscreen: new Button(this.canvas, {
        img: 'https://static.thenounproject.com/png/64334-200.png',
        alignX: 'right',
        alignY: 'bottom',
        offX: -10,
        offY: -10,
        padding: 15,
        callback: () => {
          Utils.toggleFullscreen(this.canvas)
        }
      })
    };
    this.texts = {
      fps: new Text(this.canvas, {
        text: '0',
        alignX: 'left',
        alignY: 'top',
        offX: 10,
        offY: 10,
        font: {
          family: 'Courier New',
          size: '20px'
        }
      })
    };
  }

  setOpts(opts) {
    const defaultOpts = {
      debug: false,
      width: 0,
      height: 0,
      game: {
        ball: {
          radius: 5
        }
      },
    };
    this.opts = Utils.mergeDeep(defaultOpts, opts);
  }

  set activeState(state) {
    this.activeStateReal.kill();
    state.init();
    this.activeStateReal = state;
  }

  get activeState() {
    return this.activeStateReal;
  }

  run() {
    this.setDimensions();

    requestAnimationFrame(step => this.loop(step));
  }

  setDimensions() {
    const canvas = this.canvas;
    const canvasParent = this.canvas.parentElement;
    const opts = this.opts;

    if (opts.width === 0)
      window.addEventListener('resize', () => {
        canvas.width = canvasParent.clientWidth;
      });
    else
      canvas.width = opts.width;

    if (opts.height === 0)
      window.addEventListener('resize', () => {
        canvas.height = canvasParent === document.body ? window.innerHeight : canvasParent.clientHeight;
      });
    else
      canvas.height = opts.height;

    window.dispatchEvent(new Event('resize'));
  }

  loop(step) {
    this.interval = step - this.lastTickTime;

    this.update();
    this.draw();

    this.lastTickTime = step;
    requestAnimationFrame(step => this.loop(step));
  }

  update() {
    if (this.opts.debug)
    {
      this.updateDebug();
    }
    if (this.activeState instanceof Game) {
      if (!document.hasFocus())
        this.activeState = this.pause;
      this.game.update();
    }
  }

  updateDebug() {
    this.texts.fps.opts.text = `fps: ${Math.round(1000 / this.interval * 1000) / 1000}`;
  }

  draw() {
    const canvas = this.canvas;
    const ctx = this.ctx;

    ctx.fillStyle = 'white';
    ctx.fillRect(0, 0, canvas.width, canvas.height);

    if (this.activeState instanceof Menu) {
      this.menu.draw();
    } else if (this.activeState instanceof Game) {
      this.game.draw();
    } else if (this.activeState instanceof Pause) {
      this.game.draw();
      this.pause.draw();
    }

    if (this.opts.debug) {
      this.debugDraw();
    }
    this.buttons.fullscreen.draw();
  }

  debugDraw() {
    this.texts.fps.draw();
  }
}

class State {
  constructor() {
    if (new.target === State)
      throw new TypeError("Cannot construct State instances directly");
    if (typeof this.init !== "function")
      throw new TypeError("Must override method");
    if (typeof this.update !== "function")
      throw new TypeError("Must override method");
    if (typeof this.draw !== "function")
      throw new TypeError("Must override method");
    if (typeof this.kill !== "function")
      throw new TypeError("Must override method");
  }

  kill() {
    this.disableButtons();
  }

  disableButtons() {
    if (typeof this.buttons === "object")
      Object.keys(this.buttons).forEach(e => e.disable());
  }
}

class Menu extends State {
  constructor(pung) {
    super();
    this.pung = pung;
    this.buttons = {
      play: new Button(pung.canvas, {
        text: 'Play',
        border: true,
        padding: 10,
        callback: () => {
          pung.activeState = pung.game;
          pung.game.start();
        }
      }),
    }
  }

  init() {
    const buttons = this.buttons;
    buttons.play.enable();
  }

  update() {

  }

  draw() {
    const buttons = this.buttons;
    buttons.play.draw();
  }

  kill() {
    const buttons = this.buttons;
    buttons.play.disable();
  }
}

class Game extends State {
  constructor(pung) {
    super();
    this.pung = pung;
    this.opts = pung.opts.game;
    this.ball = new Ball(this);
  }

  init() {

  }

  start() {
    const ball = this.ball;
    const pung = this.pung;
    const canvas = pung.canvas;
    pung.state = 1;
    ball.x = canvas.width / 2;
    ball.y = canvas.height / 2;
  }

  resume() {
    const pung = this.pung;
    pung.state = 1;
  }

  update() {
    this.ball.update();
  }

  draw() {
    this.ball.draw();
  }

  kill() {

  }
}

class Ball {
  constructor(game) {
    this.game = game;
    this.opts = game.opts.ball;
    this.x = 0;
    this.y = 0;
    this.dirX = 1;
    this.dirY = 1;
  }

  update() {
    const pung = this.game.pung;
    const canvas = pung.canvas;
    const interval = pung.interval;
    const radius = this.opts.radius;

    this.x += Math.floor(this.dirX * (interval) / 4);
    this.y += Math.floor(this.dirY * (interval) / 4);

    if (this.x <= radius)
    {
      this.dirX *= -1;
      this.x = radius;
    }
    else if (this.x >= canvas.width - radius)
    {
      this.dirX *= -1;
      this.x = canvas.width - radius;
    }

    if (this.y <= radius)
    {
      this.dirY *= -1;
      this.y = radius;
    }
    else if (this.y >= canvas.height - radius)
    {
      this.dirY *= -1;
      this.y = canvas.height - radius;
    }

  }

  draw() {
    const ctx = this.game.pung.ctx;
    const opts = this.opts;
    ctx.beginPath();
    ctx.arc(this.x, this.y, opts.radius, 0, Math.PI * 2);
    ctx.fillStyle = 'black';
    ctx.fill();
  }
}

class Pause extends State {
  constructor(pung) {
    super();
    this.pung = pung;
    this.buttons = {
      resume: new Button(pung.canvas, {
        text: 'Resume',
        border: true,
        padding: 10,
        callback: () => {
          pung.activeState = pung.game;
          pung.game.resume();
        }
      }),
    }
  }

  init() {
    const buttons = this.buttons;
    buttons.resume.enable();
  }

  update() {

  }

  draw() {
    const buttons = this.buttons;
    buttons.resume.draw();
  }

  kill() {
    const buttons = this.buttons;
    buttons.resume.disable();
  }
}

class Text {
  constructor(canvas, opts) {
    this.canvas = canvas;
    this.ctx = this.canvas.getContext('2d');
    const ctx = this.ctx;

    this.setOpts(opts);
    opts = this.opts;

    ctx.font = this.font;
    this.textWidth = ctx.measureText(opts.text).width;
    this.textHeight = parseFloat(opts.font.size);

    this.borderWidth = this.textWidth + opts.padding * 2;
    this.borderHeight = this.textHeight * 1.4 + opts.padding * 2;
  }

  setOpts(opts) {
    const defaultOpts = {
      text: null,
      img: null,
      alignX: 'center',
      alignY: 'center',
      offX: 0,
      offY: 0,
      padding: 0,
      border: false,
      font: {
        style: 'normal',
        variant: 'normal',
        weight: 'normal',
        size: '60px',
        family: 'Comic Sans MS',
        color: 'black'
      }
    };
    this.opts = Utils.mergeDeep(defaultOpts, opts);
    if (this.opts.text === null) {
      this.opts.font.size = '0px';
    }
  }

  get x() {
    const opts = this.opts;
    const canvas = this.canvas;

    let x;
    if (opts.alignX === 'left')
      x = this.borderWidth / 2;
    else if (opts.alignX === 'center')
      x = canvas.width / 2;
    else if (opts.alignX === 'right')
      x = canvas.width - this.borderWidth / 2;

    return x + opts.offX;
  }

  get y() {
    const opts = this.opts;
    const canvas = this.canvas;

    let y;
    if (opts.alignY === 'top')
      y = this.borderHeight / 2;
    else if (opts.alignY === 'center')
      y = canvas.height / 2;
    else if (opts.alignY === 'bottom')
      y = canvas.height - this.borderHeight / 2;

    return y + opts.offY;
  }

  get textX() {
    return this.x - this.textWidth / 2;
  }

  get textY() {
    return this.y + this.textHeight / 3;
  }

  get borderX() {
    return this.textX - this.opts.padding;
  }

  get borderY() {
    return this.y - this.textHeight / 1.3 - this.opts.padding;
  }

  draw() {
    const ctx = this.ctx;
    const opts = this.opts;
    ctx.beginPath();
    if (opts.img !== null) {
      const img = new Image();
      img.src = opts.img;
      ctx.drawImage(img, this.borderX, this.borderY, this.borderWidth, this.borderHeight);
    }
    if (opts.text !== null) {
      ctx.font = this.font;
      ctx.fillStyle = opts.font.color;
      ctx.fillText(this.opts.text, this.textX, this.textY);
    }
    opts.border ? ctx.strokeRect(this.borderX, this.borderY, this.borderWidth, this.borderHeight) : null;
  }

  get font() {
    const {style, variant, weight, size, family} = this.opts.font;
    return `${style} ${variant} ${weight} ${size} ${family}`;
  }
}

class Button extends Text {
  constructor(canvas, opts) {
    super(canvas, opts);

    this.click = (e) => {
      if (this.isMouseIn(e) && this.disabled === false && typeof this.opts.callback === "function")
        this.opts.callback();
    };

    this.enable();
  }

  enable() {
    this.disabled = false;
    canvas.addEventListener('click', this.click);
  }

  disable() {
    this.disabled = true;
    canvas.removeEventListener('click', this.click);
  }

  isMouseIn(e) {
    return e.clientX > this.borderX &&
      e.clientX < this.borderX + this.borderWidth &&
      e.clientY > this.borderY &&
      e.clientY < this.borderY + this.borderHeight;
  }
}

class Utils {
  static toggleFullscreen(elem) {
    if (!document.fullscreenElement) {
      if (elem.requestFullscreen) {
        elem.requestFullscreen();
      } else if (elem.mozRequestFullScreen) { /* Firefox */
        elem.mozRequestFullScreen();
      } else if (elem.webkitRequestFullscreen) { /* Chrome, Safari and Opera */
        elem.webkitRequestFullscreen();
      } else if (elem.msRequestFullscreen) { /* IE/Edge */
        elem.msRequestFullscreen();
      }
    } else {
      if (document.exitFullscreen) {
        document.exitFullscreen();
      } else if (document.mozCancelFullScreen) { /* Firefox */
        document.mozCancelFullScreen();
      } else if (document.webkitExitFullscreen) { /* Chrome, Safari and Opera */
        document.webkitExitFullscreen();
      } else if (document.msExitFullscreen) { /* IE/Edge */
        document.msExitFullscreen();
      }
    }
  }

  static isObject(item) {
    return (item && typeof item === 'object' && !Array.isArray(item));
  }

  static mergeDeep(target, ...sources) {
    if (!sources.length) return target;
    const source = sources.shift();

    if (Utils.isObject(target) && Utils.isObject(source)) {
      for (const key in source) {
        if (Utils.isObject(source[key])) {
          if (!target[key]) Object.assign(target, {[key]: {}});
          Utils.mergeDeep(target[key], source[key]);
        } else {
          Object.assign(target, {[key]: source[key]});
        }
      }
    }

    return Utils.mergeDeep(target, ...sources);
  }
}


